<h1>Curso Desenvolvimento WEB</h1>
Arquivos do curso de Desenvolvimento WEB da SETIC 2019.
<br/><br/>
Documentação: https://hackmd.io/@q4vNGEetSiSCXxQ00iqFHQ/ry-cVCGdH para ler depois =D
<br/>
Slides: https://drive.google.com/file/d/1qFXFnB2nNPk-zxGn5R30b2l8Ou9Y9QkC/view?usp=sharing
<br/>
Template: https://colorlib.com/wp/template/ronaldo/
<br/><br/>
Entre no grupo do Telegram dos alunos: https://t.me/joinchat/GJmWkxMkJygh1zoMv7xfoA =)
<br/>
CodeSandBox: https://codesandbox.io ou utilize o VSCODE ou o Sublime e vamos começar a resenha!!!
<br/>
Feito com muito 💜 pela GTi Engenharia Jr.